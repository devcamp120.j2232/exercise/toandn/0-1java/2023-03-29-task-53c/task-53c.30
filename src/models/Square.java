package models;

public class Square extends Rectangle {

  public Square() {
  }

  public Square(double side) {
    this.length = side;
    this.width = side;
  }

  public Square(String color, Boolean filled, double side) {
    super(color, filled);
    this.length = side;
    this.width = side;
  }  
  
  public double getSide() {
    return this.length;
  }

  public void setSide(double side) {
      this.length = side;
      this.width = side;
  }

  public void setWidth(double side) {
    this.width = side;
  }

  public void setLength(double side) {
    this.length = side;
  }

  @Override
    public String toString() {
      return "Square[Rectangle[color="+ this.getColor() + ", filled=" + this.isFilled() + "],length=" + length + ", width=" + width + "]";
    }
  
}
